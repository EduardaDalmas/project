﻿using System;

namespace Project
{
     class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();
            Console.Write("Digite um número: ");
            int num1 = int.Parse(Console.ReadLine());
            Console.Write("Digite o segundo número: ");
            int num2 = int.Parse(Console.ReadLine());
            Console.Write("Digite 1 para soma: ");
            Console.Write("Digite 2 para subtração: ");
            Console.Write("Digite 3 para multiplicação: ");
            Console.Write("Digite 4 para divisão: ");
            int opcao = int.Parse(Console.ReadLine());
            if (opcao  == 1) {
                Console.WriteLine("O resultado é: {0}", Calculadora.Soma(num1, num2));
            }
            if (opcao  == 2) {
                Console.WriteLine("O resultado é: {0}", Calculadora.Subtração(num1, num2));
            }
            if (opcao  == 3) {
                Console.WriteLine("O resultado é: {0}", Calculadora.Multiplicação(num1, num2));
            }
            if (opcao  == 4) {
                Console.WriteLine("O resultado é: {0}", Calculadora.Divisão(num1, num2));
            }
        }
    }
}