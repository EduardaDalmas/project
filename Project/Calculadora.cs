 namespace Project{
    public class Calculadora
    {
        public static int Soma(int numero1, int numero2)
        {
            return numero1 + numero2;
        }

        public static int Subtração(int numero1, int numero2)
        {
            return numero1 - numero2;
        }

        public static int Divisão(int numero1, int numero2)
        {
            return numero1 / numero2;
        }

        public static int Multiplicação(int numero1, int numero2)
        {
            return numero1 * numero2;
        }
    }
}